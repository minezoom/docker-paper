FROM openjdk:8-jre-alpine

MAINTAINER Keenan Thompson <keenan@keenant.com>

ENV PAPER_HOME=/minecraft \
    PAPER_VER=lastSuccessfulBuild \
    PAPER_USER=minecraft \
    PAPER_UID=-1

COPY rootfs /

RUN apk update && \

    # bash, wget, rc, ps, tail (and others)
    apk add bash wget openrc procps coreutils screen unzip


EXPOSE 25565

ENTRYPOINT /paper_setup.sh && \
           /bin/bash