#!/bin/bash
PAPER_UID="$PAPER_UID"

if [ "$PAPER_UID" = "-1" ] && [ -d "$PAPER_HOME" ]; then
  PAPER_UID=$(stat -c '$u' "$PAPER_HOME")
  echo "Using $PAPER_HOME uid ($PAPER_UID)"
else
  if [ -d "$PAPER_HOME" ]; then
    PAPER_UID=$(stat -c '$u' "$PAPER_HOME")
  fi
fi

export PAPER_UID

if [ "$PAPER_UID" = "-1" ]; then
  mkdir /minecraft
  PAPER_USER=root
  export PAPER_USER
else
  adduser minecraft -D -g "" -h "$PAPER_HOME" -u "$PAPER_UID"
fi 

service minecraft start
