# docker-paper

A lightweight Docker image running the Paper edition of Minecraft server software.

## Usage

```shell
docker run -i -t -e EULA=true registry.gitlab.com/minezoom/docker-paper
```

Control the Minecraft server using the service, `service minecraft <cmd>` or `/etc/init.d/minecraft <cmd>` where `<cmd>`
is one of the following:

* `start` to start the server
* `stop` to stop the server
* `restart` to `stop` the server, then `start` it again
* `status` to display if the server is running or not
* `console` to enter the interactive Minecraft console through a `screen` session (exit using Ctrl+A then Ctrl+D)
* `log` to listen to `tail` the logs of the server
* `send` followed by a minecraft command, like `send op keenanjt` to execute a Minecraft console command

## ...

More documentation to come (volumes, uids, etc)...